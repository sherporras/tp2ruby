require_relative "soporte"
require_relative "generarRegMovimiento"

module Alquilable   

    def puede_alquilar?
        !@alquilada      
    end

    def alquilar(socio)
        if puede_alquilar?
          precio=socio.calcularPrecioPelicula(self)
          puts"#{socio.nombre} alquilo #{titulo} con precio  #{precio}" 
          @alquilada=true
          GenerarRegMovimiento.registrarAlquiler(socio,self,precio)
        else
            puts "ya esta alquilada"
        end              
    end

    def devolver
        if @alquilada
            @alquilada=false          
        end      
    end

end

class Pelicula < Soporte
  include Alquilable
  attr_accessor :titulo,:precioAlquiler,:alquilada
  
  def initialize (tipo,titulo,precio)
    super(tipo)
    @titulo=titulo
    @precioAlquiler=precio
    @alquilada=false
     
  end

end
