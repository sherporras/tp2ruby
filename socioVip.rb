require_relative "socio"

class SocioVip<Socio
  def calcularPrecioPelicula(pelicula)
    descuento=0.1
    pelicula.precioAlquiler*(1-descuento)
  end
end
