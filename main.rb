require_relative "socioVip"
require_relative "socioNormal"
require_relative "pelicula"

#Socios:
socioVip1=SocioVip.new("SocioVipUno")
socioVip2=SocioVip.new("SocioVipDos")
socioVip3=SocioVip.new("SocioVipTres")
socioVip4=SocioVip.new("SocioVipCuatro")
socioVip5=SocioVip.new("SocioVipCinco")

socioNormal1=SocioNormal.new("SocioNormalUno")
socioNormal2=SocioNormal.new("SocioNormalDos")
socioNormal3=SocioNormal.new("SocioNormalTres")
socioNormal4=SocioNormal.new("SocioNormalCuatro")
socioNormal5=SocioNormal.new("SocioNormalCinco")
#Peliculas:
pelicula1=Pelicula.new("DVD","Pelicula1",150.0)
pelicula2=Pelicula.new("DVD","Pelicula2",250.0)
pelicula3=Pelicula.new("Video","Pelicula3",50.0)
pelicula4=Pelicula.new("Video","Pelicula4",350.0)
pelicula5=Pelicula.new("Video","Pelicula5",550.0)
pelicula6=Pelicula.new("DVD","Pelicula6",150.0)
pelicula7=Pelicula.new("DVD","Pelicula7",250.0)
pelicula8=Pelicula.new("BlueRay","Pelicula8",50.0)
pelicula9=Pelicula.new("BlueRay","Pelicula9",350.0)
pelicula10=Pelicula.new("BlueRay","Pelicula10",550.0)



socioVip1.alquilarPelicula(pelicula1)
socioVip2.alquilarPelicula(pelicula1) #ya esta alquilada

socioVip2.alquilarPelicula(pelicula2)
socioVip3.alquilarPelicula(pelicula3)
socioVip4.alquilarPelicula(pelicula4)
socioVip5.alquilarPelicula(pelicula5)
socioNormal1.alquilarPelicula(pelicula6)
socioNormal2.alquilarPelicula(pelicula7)
socioNormal3.alquilarPelicula(pelicula8)
socioNormal4.alquilarPelicula(pelicula9)
socioNormal5.alquilarPelicula(pelicula10)